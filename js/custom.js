
var doItParallax = function () {

    var doItTop =  $('#do-it').offset().top;

    var wHeight = $(window).height();

    var wScroll = $(window).scrollTop();

    var wWidth = $(window).width();


    if (wScroll > doItTop - wHeight/1.2) {

        if (wWidth > 768) {

            $('#do-it .row-custom .item:first').css({
                'transform': 'translate3d(0px, '+ -(wScroll/20 - doItTop/21) +'%, 0px)'
            });

            $('#do-it .row-custom .item:nth-child(2)').css({
                'transform': 'translate3d(0px, '+ (wScroll/65 - doItTop/66) +'%, 0px)'
            });

            $('#do-it .row-custom .item:nth-child(3)').css({
                'transform': 'translate3d(0px, '+ (wScroll/35 - doItTop/36) +'%, 0px)'
            });

            $('#do-it .row-custom .item:nth-child(4)').css({
                'transform': 'translate3d(0px, '+ (wScroll/64 - doItTop/65) +'%, 0px)'
            });

            $('#do-it .row-custom .item:last-child').css({
                'transform': 'translate3d(0px, '+ -(wScroll/70 - doItTop/71 - 25) +'%, 0px)'
            });
        } else {
            $('#do-it .row-custom .item').css({
                'transform': 'translate3d(0px, 0px, 0px)'
            });
        }

    }

};


$(document).scroll(function () {

    doItParallax();

});


$(document).resize(function () {

    doItParallax();

});




$(document).ready(function () {

    doItParallax();

    var stickyNav = function () {

        var wScrollTop = $(window).scrollTop();

        if(wScrollTop > 0) {

            $('#navigation').addClass('sticky');

        } else {

            $('#navigation').removeClass('sticky');

        }

    };

    stickyNav();

    $(window).scroll(function () {

        stickyNav();

    });


});





